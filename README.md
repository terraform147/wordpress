# Terraform Wordpress

## Contains

- Create an EC2 with Wordpress

## How to use

### Setup Module
module "wordpress" {
  source                = "git@gitlab.com:terraform147/wordpress.git"
  domain                = ""
  instance_type         = ""
  name                  = ""
  pem                   = ""
  region                = ""
  wordpress_bitnami_ami = ""
}

### Import module

```
terraform init
```

## Give a star :stars:

## Want to improve or fix something? Fork me and send a MR! :punch: