resource "aws_lb_target_group" "this" {
  name        = "${var.name}-wp-lb-tg"
  port        = 80
  protocol    = "HTTP"
  vpc_id      = data.aws_vpc.selected.id
  target_type = "instance"

  lifecycle {
    create_before_destroy = true
  }

  health_check {
    matcher  = "200"
    path     = "/"
    port     = 80
    timeout  = 30
    interval = 40
  }

  tags = {
    Product = var.name
  }
}

resource "aws_lb_listener" "https" {
    certificate_arn   = data.aws_acm_certificate.selected.arn
  load_balancer_arn = data.aws_lb.external.arn
  port              = 443
  protocol          = "HTTPS"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.this.arn
  }
}
