resource "aws_route53_record" "this" {
  zone_id = data.aws_route53_zone.selected.id
  name    = "blog.${var.domain}"
  type    = "A"
  alias {
    name                   = data.aws_lb.external.dns_name
    zone_id                = data.aws_lb.external.zone_id
    evaluate_target_health = true
  }
}
